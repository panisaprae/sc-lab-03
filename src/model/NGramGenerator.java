package model;

import java.util.ArrayList;
import java.util.List;

public final class NGramGenerator {
	
	public static String[] generateNgram( String str ){
		str = str.replace(" ", "");
		List<String> Ngrams = new ArrayList<String>();
		for ( int i = 0 ; i < str.length() - 2 ; i++ ) {
			Ngrams.add( str.substring(i, i + 3) );
		}
		
		String[] arr = Ngrams.toArray(new String[Ngrams.size()]);
		return arr;
	}

}
