package view;

import javax.swing.JFrame; 
import javax.swing.JOptionPane;

import model.NGramGenerator;

public class GUI {
	
	public GUI() {
		initComponent();
	}
	
	public void initComponent() {
		JFrame frame = new JFrame("Ngram Generator");

	    String word = JOptionPane.showInputDialog(frame, "Input A Sentence");
	    
	    String [] Ngrams = NGramGenerator.generateNgram(word);
	    
	    String output = "The Original Sentence : " + word + "\nNgrams : ";
	    
	    for ( String s : Ngrams ) {
	    	output += " " + s;
	    }
	    
	    JOptionPane.showConfirmDialog(frame, output);
	} 

	 
}
